# Permissions
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/configs/permissions/oplus_google_lens_config.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/oplus_google_lens_config.xml \
    $(LOCAL_PATH)/configs/permissions/privapp-permissions-oplus.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oplus.xml \
    $(LOCAL_PATH)/configs/sysconfig/hiddenapi-package-oplus-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-oplus-whitelist.xml

# Oplus framework
PRODUCT_PACKAGES += \
    oplus-fwk

PRODUCT_BOOT_JARS += \
    oplus-fwk \
    coloros-support-wrapper

TARGET_USES_OPLUS_CAMERA := true

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    persist.vendor.camera.privapp.list=com.oppo.camera \
    ro.com.google.lens.oem_camera_package=com.oppo.camera \
    ro.com.google.lens.oem_image_package=com.google.android.apps.photos

PRODUCT_SYSTEM_EXT_PROPERTIES += \
    ro.oplus.camera.video_beauty.prefix=oplus.video.beauty. \
    ro.oplus.camera.video.beauty.switch=oplus.switch.video.beauty \
    ro.oplus.camera.speechassist=true \
    ro.oplus.system.camera.name=com.oppo.camera \
    ro.oplus.system.gallery.name=com.google.android.apps.photos

$(call inherit-product, vendor/oplus/camera/camera-vendor.mk)

